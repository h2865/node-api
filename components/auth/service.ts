import {sign} from "jsonwebtoken";
import HashMap from "../../hashMap";
import crypto from "crypto";
import bcrypt from "bcrypt";
import fs from "fs-extra";

interface UserModel {
	id: string,
	username: string,
	password: string
}

interface AuthDataModel {
	[userId: string]: UserModel
}

export class AuthService
{
	private static DATA_PATH = "data/auth.json"

	private users: HashMap<string, UserModel>;

	constructor() {
		this.users = this.loadDataFromDisk();
	}

	static generateAccessToken(data: any)
	{
		const accessToken = process.env.ACCESS_TOKEN as string;

		return sign(data, accessToken);
	}

	static getHashedPassword(password)
	{
		const salt = bcrypt.genSaltSync(10);
		return bcrypt.hashSync(password, salt);
	}

	loadDataFromDisk(): HashMap<string, UserModel>
	{
		const fileDataExists = fs.existsSync(AuthService.DATA_PATH);

		if(!fileDataExists) {
			return new HashMap<string, UserModel>()
		}

		const data: AuthDataModel = fs.readJsonSync(AuthService.DATA_PATH);
		const dataEntries = Object.entries(data);

		const result = new HashMap<string, UserModel>();

		dataEntries.forEach(([ userId, userData ]) => {
			result.set(userId, userData)
		})

		return result;
	}

	persistDataToDisk()
	{
		const dataToSave: AuthDataModel = {};

		this.users.keys.forEach(userId => dataToSave[userId.toString()] = this.users.get(userId))

		fs.outputJSONSync(AuthService.DATA_PATH, dataToSave);
	}

	findUserByUsernameAndPassword(username: string, password: string): { accessToken: string, user: UserModel }
	{
		const user = Array.from(this.users.keys)
			.map(el => this.users.get(el))
			.find(el => {
				console.log(el.password)
				console.log(password)
				return el.username === username && bcrypt.compareSync(password, el.password)
			});

		if(!user) {
			return null;
		}

		const accessToken = AuthService.generateAccessToken({ user: user.id });

		return { accessToken, user }
	}

	findUserById(userId: string) {
		return this.users.get(userId);
	}

	registerUser(username: string, password: string)
	{
		const alreadyExists = (Array.from(this.users.keys)
			.map(el => this.users.get(el))
			.find(el => el.username === username))!!;

		if(alreadyExists) {
			return false;
		}

		const hashedPassword = AuthService.getHashedPassword(password);
		const newUser: UserModel = {
			id: crypto.randomBytes(16).toString("hex"),
			username,
			password: hashedPassword
		}

		this.users.set(newUser.id, newUser);

		return true;
	}
}
