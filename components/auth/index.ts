import { Router, Request, Response } from 'express';
import { AuthService } from "./service";
import { authenticateToken } from "../../middlewares/authMiddleware";

const authRouter = Router();
const service = new AuthService();

authRouter.route('/login').post((req: Request, res: Response) =>
{
	const { username, password } = req.body;

	const results = service.findUserByUsernameAndPassword(username, password);

	if(!results) {
		return res.sendStatus(401);
	}

	const productionEnv = process.env.NODE_ENV?.trim() !== 'development'

	res.cookie('accessToken', results.accessToken, {
		maxAge: (60 * 1000) * 120,
		httpOnly: true,
		secure: productionEnv
	});

	res.json({ user: { id: results.user.id, username: results.user.username }, accessToken: results.accessToken })
});

authRouter.route('/signup').post((req: Request, res: Response) =>
{
	const { username, password } = req.body;

	const created = service.registerUser(username, password);

	if(!created) {
		return res.sendStatus(400);
	}

	res.sendStatus(204);
});

authRouter.route('/get-user').get(authenticateToken, (req: Request, res: Response) => {
	const user = service.findUserById(req.userId);
	user ? res.json(user) : res.sendStatus(401);
});

authRouter.route('/logout').post(authenticateToken, (req: Request, res: Response) => {
	res.clearCookie('accessToken');
	res.sendStatus(204)
});

module.exports = {
	authRouter,
	authOnExit: () => service.persistDataToDisk()
};
