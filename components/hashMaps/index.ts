import { Router, Request, Response } from 'express';
import { HashMapService } from "./service";
import { authenticateToken } from "../../middlewares/authMiddleware";
import { logUserAction } from "../../middlewares/billingMiddleware";

const hashMapRouter = Router();
const service = new HashMapService();

hashMapRouter.route('/').get([authenticateToken, logUserAction], (req: Request, res: Response) => {
	const userId = req.userId;

	if(!userId) {
		return res.sendStatus(400);
	}

	const responseData = service.retrieveUserHashMap(String(userId));
	res.send(responseData);
});

hashMapRouter.route('/:id').get([authenticateToken, logUserAction], (req: Request, res: Response) => {
	const userId = req.userId;
	const { id } = req.params;

	if(!userId) {
		return res.sendStatus(400);
	}

	const responseData = service.retrieveUserHashMapById(String(userId), parseInt(id));

	if(!responseData) {
		return res.sendStatus(404);
	}

	res.send(responseData);
});

hashMapRouter.route('/').post([authenticateToken, logUserAction], (req: Request, res: Response) => {
	const userId = req.userId;
	const { title } = req.body;

	if(!userId || !title) {
		return res.sendStatus(400);
	}

	const newHashMap = service.createUserHashMap(String(userId), String(title));
	res.send(newHashMap);
});

hashMapRouter.route('/:id/data').put([authenticateToken, logUserAction], (req: Request, res: Response) => {
	const userId = req.userId;
	const { id } = req.params;
	const data = req.body;

	if(!userId || isNaN(parseInt(id)) || !data) {
		return res.sendStatus(400);
	}

	service.putDataUserHashMap(String(userId), parseInt(id), data);
	res.sendStatus(204)
});

hashMapRouter.route('/:id').delete([authenticateToken, logUserAction], (req: Request, res: Response) => {
	const userId = req.userId;
	const { id } = req.params;

	if(!userId || isNaN(parseInt(id))) {
		return res.sendStatus(400);
	}

	const success = service.removeUserHashMap(String(userId), parseInt(id));
	res.sendStatus(success ? 204 : 400);
});

hashMapRouter.route('/entry').put([authenticateToken, logUserAction], (req: Request, res: Response) => {
	const { hashMapId } = req.query;
	const userId = req.userId;
	const entry: { key: String, value: String } = req.body;

	if(!userId || !hashMapId) {
		return res.sendStatus(400);
	}

	const success = service.addUserEntry(String(userId), parseInt(hashMapId.toString()), entry);
	res.sendStatus(success ? 204 : 400);
});

hashMapRouter.route('/entry/:key').delete([authenticateToken, logUserAction], (req: Request, res: Response) => {
	const { hashMapId } = req.query;
	const { key } = req.params;
	const userId = req.userId;

	if(!userId || !hashMapId || !key) {
		return res.sendStatus(400);
	}

	const success = service.removeUserEntry(String(userId), parseInt(hashMapId.toString()), decodeURI(key));
	res.sendStatus(success ? 204 : 400);
});

module.exports = {
	hashMapRouter,
	hashMapOnExit: () => service.persistDataToDisk()
};
