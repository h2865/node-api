import HashMap from "../../hashMap";
import fs from 'fs-extra'

interface UserHashMapPOJO {
	id: number;
	title: String;
	data: { key: String, value: String }[];
}

interface HashMapModel {
	id: number;
	title: String;
	hashMap: HashMap<String, String>;
}

interface UserHashMapDataModel
{
	[userId: string]: {
		[hashMapId: string]: {
			id: number,
			title: String,
			data: { key: String, value: String }[];
		}
	}
}

export class HashMapService
{
	private static DATA_PATH = "data/hashMaps.json"
	private usersHashMap: HashMap<String, HashMapModel[]>;

	constructor() {
		this.usersHashMap = this.loadDataFromDisk();
	}

	loadDataFromDisk(): HashMap<String, HashMapModel[]>
	{
		const fileDataExists = fs.existsSync(HashMapService.DATA_PATH);

		if(!fileDataExists) {
			return new HashMap<String, HashMapModel[]>()
		}

		const data: UserHashMapDataModel = fs.readJsonSync(HashMapService.DATA_PATH);
		const dataEntries = Object.entries(data);

		const result = new HashMap<String, HashMapModel[]>();

		dataEntries.forEach(([ userId, hashMaps ]) => {
			const userMaps: HashMapModel[] = Object.entries(hashMaps).map(([ hashMapId, dataHashMap ]) => {
				const hashMap = new HashMap<String, String>();

				dataHashMap.data.forEach(({ key, value }) => hashMap.set(key, value));

				return <HashMapModel>{
					id: parseInt(hashMapId),
					title: dataHashMap.title,
					hashMap: hashMap
				}
			});

			result.set(userId, userMaps)
		})

		return result;
	}

	persistDataToDisk()
	{
		const dataToSave: UserHashMapDataModel = {};

		this.usersHashMap.keys.forEach(userId =>
		{
			const hashMaps = this.usersHashMap.get(userId);

			dataToSave[userId.toString()] = hashMaps.reduce((obj, item) => {
				return ({
					...obj,
					[item.id.toString()]: {
						id: item.id, title: item.title,
						data: Array.from(item.hashMap.keys).map(key => {
							const value = item.hashMap.get(key);

							return {
								key,
								value
							}
						})
					}
				})
			}, {});
		})

		fs.outputJSONSync(HashMapService.DATA_PATH, dataToSave);
	}

	retrieveUserHashMap(userId: String): UserHashMapPOJO[]
	{
		const hashMaps = this.usersHashMap.get(userId);

		if(!hashMaps) {
			return [];
		}

		return hashMaps.map((el, i) => <UserHashMapPOJO> {
			id: i,
			title: el.title,
			data: Array.from(el.hashMap.keys).map(key => {
				const value = el.hashMap.get(key);

				return {
					key,
					value
				}
			})
		})
	}

	createUserHashMap(userId: string, hashMapTitle: string)
	{
		let hashMaps = this.usersHashMap.get(userId);

		if(hashMaps == null) {
			hashMaps = new Array<HashMapModel>();
		}

		const hashMapId = hashMaps.length;
		const newHashMap = {
			id: hashMapId,
			title: hashMapTitle,
			hashMap: new HashMap<String, String>()
		}

		hashMaps.push(newHashMap);

		this.usersHashMap.set(userId, hashMaps);

		return { id: newHashMap.id, title: newHashMap.title, data: [] }
	}

	addUserEntry(userId: string, hashMapId: number, entry: { key: String; value: String })
	{
		const hashMaps = this.usersHashMap.get(userId);

		if(hashMaps == null) {
			return false;
		}

		const selectedHashMap = hashMaps[hashMapId];

		if(selectedHashMap == null) {
			return false;
		}

		selectedHashMap.hashMap.set(entry.key, entry.value);
		this.usersHashMap.get(userId)[hashMapId] = selectedHashMap;

		return true;
	}

	removeUserEntry(userId: string, hashMapId: number, entryKey: string)
	{
		const hashMaps = this.usersHashMap.get(userId);

		if(hashMaps == null) {
			return false;
		}

		const selectedHashMap = hashMaps[hashMapId];

		if(selectedHashMap == null) {
			return false;
		}

		return selectedHashMap.hashMap.remove(entryKey);
	}

	removeUserHashMap(userId: string, hashMapId: number)
	{
		let hashMaps = this.usersHashMap.get(userId);

		if(hashMaps == null) {
			return false;
		}

		hashMaps = hashMaps.filter(el => el.id !== hashMapId);

		this.usersHashMap.set(userId, hashMaps);

		return true;
	}

	retrieveUserHashMapById(userId: string, hashMapId: number)
	{
		const hashMaps = this.usersHashMap.get(userId);

		if(!hashMaps) {
			return null;
		}

		const requestedHashMap = hashMaps[hashMapId];

		if(requestedHashMap == null) {
			return null;
		}

		return <UserHashMapPOJO> {
			id: requestedHashMap.id,
			title: requestedHashMap.title,
			data: Array.from(requestedHashMap.hashMap.keys).map(key => {
				const value = requestedHashMap.hashMap.get(key);

				return {
					key,
					value
				}
			})
		};
	}

	putDataUserHashMap(userId: string, hashMapId: number, data: { key: String, value: String }[])
	{
		const hashMaps = this.usersHashMap.get(userId);

		if(hashMaps == null) {
			return false;
		}

		const selectedHashMap = hashMaps[hashMapId];

		if(selectedHashMap == null) {
			return false;
		}

		selectedHashMap.hashMap = new HashMap<String, String>();
		data.forEach(({ key, value }) => selectedHashMap.hashMap.set(key, value));
		this.usersHashMap.get(userId)[hashMapId] = selectedHashMap;

		return true;
	}
}
