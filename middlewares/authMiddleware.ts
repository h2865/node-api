import {Request, Response, NextFunction} from 'express';
import { verify } from 'jsonwebtoken';

export function authenticateToken(req: Request, res: Response, next: NextFunction)
{
	if(req.method === 'OPTIONS') {
		next();
	}
	else
	{
		const { accessToken } = req.cookies;

		if(accessToken && !req.headers.authorization)
			req.headers.authorization = `Bearer ${accessToken}`;

		const authHeader = req.headers.authorization;
		const token = authHeader && authHeader.split(' ')[1];

		if(!token)
			return res.sendStatus(401);

		verify(token, process.env.ACCESS_TOKEN as string, async (err: any, tokenData: any) =>
		{
			if(err)
				return res.sendStatus(403);

			req.userId = tokenData.user;

			const productionEnv = process.env.NODE_ENV !== 'development';

			res.cookie('accessToken', accessToken, {
				maxAge: (60 * 1000) * 120,
				httpOnly: true,
				secure: productionEnv
			});

			next();
		})
	}
}
