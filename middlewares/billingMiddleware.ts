import {NextFunction, Request, Response} from "express";
import fs from 'fs-extra'

export function logUserAction(req: Request, res: Response, next: NextFunction)
{
	if(req.method === 'OPTIONS') {
		next();
	}
	else
	{
		const currentTimestamp = Date.now();
		const log = currentTimestamp + "|" + req.userId + "|" + req.method + "|" + req.originalUrl + "\n";

		fs.appendFile('logs/billing_logs.txt', log, () => {});

		next()
	}
}
