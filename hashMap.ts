class MapEntry<K, V> {
	private readonly _key: K;
	private _value: V;

	constructor(key: K, value: V) {
		this._key = key;
		this._value = value;
	}

	get key(): K {
		return this._key;
	}

	get value(): V {
		return this._value;
	}

	set value(value: V) {
		this._value = value;
	}
}

export default class HashMap<K, V>
{
	private _keys: Set<K> = new Set<K>();
	private readonly _table: Array<MapEntry<K, V>>[];
	private _size: number;

	constructor() {
		this._table = new Array(127);
		this._size = 0;
	}

	_hash(key: K) {
		const keyString = key.toString();

		let hash = 0;
		for (let i = 0; i < keyString.length; i++) {
			hash += keyString.charCodeAt(i);
		}

		return hash;
	}

	set(key: K, value: V)
	{
		const index = this._hash(key);

		if (this._table[index])
		{
			for (let i = 0; i < this._table[index].length; i++) {
				// Find the key/value pair in the chain
				if (this._table[index][i].key === key) {
					this._table[index][i].value = value;
					return;
				}
			}

			const entry = new MapEntry<K, V>(key, value);

			// not found, push a new key/value pair
			this._table[index].push(entry);
		}
		else {
			const entry = new MapEntry<K, V>(key, value);

			this._table[index] = [entry];
		}

		this._size++;
		this._keys.add(key);
	}

	get(key: K): V
	{
		const bucket = this._hash(key);

		if (this._table[bucket]) {
			for (let i = 0; i < this._table[bucket].length; i++) {
				if (this._table[bucket][i].key === key) {
					return this._table[bucket][i].value;
				}
			}
		}

		return undefined;
	}

	remove(key: K): boolean
	{
		const index = this._hash(key);

		if (this._table[index] && this._table[index].length) {
			for (let i = 0; i < this._table[index].length; i++) {
				if (this._table[index][i].key === key) {
					this._table[index].splice(i, 1);
					this._size--;
					this._keys.delete(key);
					return true;
				}
			}
		} else {
			return false;
		}
	}

	display() {
		this._table.forEach((values, index) => {
			const chainedValues = values.map(
				(el) => `[ ${el.key}: ${el.value} ]`
			);

			console.log(`${index}: ${chainedValues}`);
		});
	}

	get table(): Array<MapEntry<K, V>>[] {
		return this._table;
	}

	get size(): number {
		return this._size;
	}

	get keys(): Set<K> {
		return this._keys;
	}
}
