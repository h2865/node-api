import 'jest';
import HashMap from "../hashMap";
import exp from "constants";

describe('HashMap', () => {
	let hashMap: HashMap<String, String>;

	beforeEach(() => {
		hashMap = new HashMap<String, String>();
	});

	it('should be empty', () =>
	{
		expect(hashMap).toBeInstanceOf(HashMap);
		expect(hashMap).toBeDefined();

		expect(hashMap.size).toBe(0);
	});

	it('get not existing key', () =>
	{
		expect(hashMap).toBeInstanceOf(HashMap);
		expect(hashMap).toBeDefined();

		expect(hashMap.size).toBe(0);

		expect(hashMap.get("key1")).toBe(undefined);
	});

	it('set/get collision case', () =>
	{
		expect(hashMap).toBeInstanceOf(HashMap);
		expect(hashMap).toBeDefined();

		hashMap.set("key1", "value1");
		hashMap.set("1key", "value2");

		expect(hashMap.size).toBe(2);

		expect(hashMap.get("key1")).toBe("value1");
		expect(hashMap.get("1key")).toBe("value2");

		expect(hashMap.table[378]).toBeDefined();
		expect(hashMap.table[0]).toBe(undefined);
	});

	it('set/remove collision case', () =>
	{
		expect(hashMap).toBeInstanceOf(HashMap);
		expect(hashMap).toBeDefined();

		hashMap.set("key1", "value1");
		hashMap.set("1key", "value2");

		expect(hashMap.size).toBe(2);

		expect(hashMap.get("key1")).toBe("value1");
		expect(hashMap.get("1key")).toBe("value2");

		expect(hashMap.table[378]).toBeDefined();
		expect(hashMap.table[0]).toBe(undefined);

		expect(hashMap.remove("key1")).toBe(true);
		expect(hashMap.size).toBe(1);
	});

	it('set/get single entry', () =>
	{
		expect(hashMap).toBeInstanceOf(HashMap);
		expect(hashMap).toBeDefined();

		hashMap.set("key1", "value1");

		expect(hashMap.size).toBe(1);

		expect(hashMap.get("key1")).toBe("value1");
	});

	it('override single entry', () =>
	{
		expect(hashMap).toBeInstanceOf(HashMap);
		expect(hashMap).toBeDefined();

		hashMap.set("key1", "value1");

		expect(hashMap.size).toBe(1);
		expect(hashMap.get("key1")).toBe("value1");

		hashMap.set("key1", "value2");
		expect(hashMap.size).toBe(1);
		expect(hashMap.get("key1")).toBe("value2");
	});

	it('set/remove single entry', () =>
	{
		expect(hashMap).toBeInstanceOf(HashMap);
		expect(hashMap).toBeDefined();

		hashMap.set("key1", "value1");

		expect(hashMap.size).toBe(1);
		expect(hashMap.get("key1")).toBe("value1");
		expect(hashMap.remove("key1")).toBe(true);
		expect(hashMap.size).toBe(0);
	});

	it('remove non existing entry', () =>
	{
		expect(hashMap).toBeInstanceOf(HashMap);
		expect(hashMap).toBeDefined();
		expect(hashMap.size).toBe(0);

		expect(hashMap.remove("key1")).toBe(false);
	});

	it('keys check', () =>
	{
		expect(hashMap).toBeInstanceOf(HashMap);
		expect(hashMap).toBeDefined();

		hashMap.set("key1", "value1");
		hashMap.set("1key", "value2");

		expect(hashMap.size).toBe(2);
		expect(hashMap.keys.size).toBe(2);
		expect(hashMap.keys).toBeInstanceOf(Set)
		expect(hashMap.keys).toEqual(new Set(["key1", "1key"]));
	});

	it('display check', () => {
		console.log = jest.fn();

		expect(hashMap).toBeInstanceOf(HashMap);
		expect(hashMap).toBeDefined();

		hashMap.set("key1", "value1");
		hashMap.display();

		expect(console.log).toHaveBeenCalledWith("378: [ key1: value1 ]");
	});
});
