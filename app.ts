import express from 'express';
import cors from "cors";
import bodyParser from "body-parser";
import morgan from "morgan";
import compression from "compression";
import helmet from "helmet";
import cookieParser from "cookie-parser";
import fs from "fs-extra";

const { hashMapOnExit } = require("./components/hashMaps")
const { authOnExit } = require("./components/auth")

require('dotenv').config();

const app = express();
const server = require('http').Server(app);

const { hashMapRouter } = require('./components/hashMaps');
const { authRouter } = require('./components/auth');

app.use(cookieParser());
app.use(compression());
app.use(helmet());

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(morgan(':method :url :status :response-time ms'));

app.use(cors({
	credentials: true,
	origin: 'http://localhost:3000',
}));

app.use('/hash-maps', hashMapRouter);
app.use('/auth', authRouter);

fs.ensureDir("logs").then(() =>
{
	server.listen(process.env.PORT, process.env.HOST, () => {
		console.log(`API started in ${process.env.NODE_ENV?.trim()} mode on: ${process.env.HOST}:${process.env.PORT}`)
	})
})

process.on('SIGINT', () =>
{
	Promise.all([hashMapOnExit(), authOnExit()])
		.then(() => process.exit(-1));
});
